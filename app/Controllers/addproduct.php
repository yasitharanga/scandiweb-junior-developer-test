<?php

require_once '../app/Models/Product.php';
require_once '../app/Models/Products/ProductParent.php';
require_once '../app/Models/Products/DVD.php';
require_once '../app/Models/Products/Book.php';
require_once '../app/Models/Products/Furniture.php';

class AddProduct extends Controller
{
    public function index()
    {
        if($_SERVER['REQUEST_METHOD'] == 'POST')
        {
            $type = $_POST['productType'];
            $product = match($type){
                'DVD' => new DVD,
                'Book' => new Book,
                'Furniture' => new Furniture,
            };
        }
        $this->view("addproduct");
    }

}
