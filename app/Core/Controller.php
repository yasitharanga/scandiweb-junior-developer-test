<?php

class Controller
{
    public function view($path, $data=[])
    {
        if(file_exists('../app/Views/' . $path . '.php'))
        {
            include('../app/Views/' . $path . '.php');
        }
        
    }

}