<?php

    class Database
    {
        private $serverName;
        private $username;
        private $password;
        private $dbName;

        protected function connect()
        {
            $this->serverName = 'localhost';
            $this->username = 'yasitha_scandiweb';
            $this->password = 'test642education';
            $this->dbName = 'yasitha_scandiweb';

            $conn = mysqli_connect(
                $this->serverName,
                $this->username,
                $this->password,
                $this->dbName
            );

            return $conn;
        }

        public function getConnection()
        {
            $connection = $this->connect();

            return $connection;
        }
    }
