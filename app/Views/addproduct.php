<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

        <title>Product Add</title>
        <script src="<?php echo ASSETS?>changeType.js" defer></script>
    </head>
    <body>

        <form method="POST" id="product_form">

            <div class="container">
                <div class="row">
                    <div class="col-12 d-flex flex-row mt-3">
                        <div class="col-9">
                            <h1>Product Add</h1>
                        </div>
                        <div class="col-3 mt-2">
                            <button type="submit" name="submit" class="btn btn-success">Save</button>
                            <a href="<?php echo ROOT?>"><button type="button" class="btn btn-danger">Cancel</button></a>
                        </div>
                    </div>

                </div>
            </div>
            <hr>

            <div class="container">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group my-3">
                            <label for="sku">SKU</label>
                            <input type="text" class="form-control" id="sku" name="sku" required>
                        </div>
                        <div class="form-group my-3">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" name="name" required>
                        </div>
                        <div class="form-group my-3">
                            <label for="price">Price</label>
                            <input type="number" step="0.01" class="form-control" id="price" name="price" required>
                        </div>
                        <div class="form-group my-3">
                            <label for="productType">Type</label>
                            <select class="form-control" id="productType" name="productType" onChange="getType(this.value)" required>
                                <option value='DVD' id="DVD" selected>DVD</option>
                                <option value='Book' id="Book">Book</option>
                                <option value='Furniture' id="Furniture">Furniture</option>
                            </select>
                        </div>
                        <div class="selectType">
                            <div class="form-group my-3" id="attributes">
                                <label for="size">Size (MB)</label>
                                <input type="number" class="form-control" name="size" id="size" required>
                                <label for="size">Please provide DVD size in MB</label>
                            </div>
                        </div>
                            
                    </div>    
                    
                </div>
            </div>

        </form>

        <!-- footer goes here -->
        <hr>
        <div class="col-12 text-center">
            <p>Scandiweb Test Assignment</p>
        </div>

        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    </body>
</html>