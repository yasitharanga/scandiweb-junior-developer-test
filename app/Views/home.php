<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <title>Product List</title>
    </head>
    <body>
        <form method="POST">
            <div class="container">
                <div class="row">
                    <div class="col-12 d-flex flex-row mt-3">
                        <div class="col-9">
                            <h1>Product List</h1>
                        </div>
                        <div class="col-3 mt-2">
                            <a href="addproduct"><button type="button" class="btn btn-success">ADD</button></a>
                            <button type="submit" class="btn btn-danger" id="delete-product-btn" name="delete-product-btn">MASS DELETE</button>
                        </div>
                    </div>
                </div>
            </div>
            <hr>

            <div class="container">
                <div class="row">
                <?php
                    require_once '../app/Models/Products.php';
                    $products = new Products();
                    $products->showAllProducts();
                ?>
                </div>
            </div>
        </form>

<!-- footer goes here -->
<hr>
<div class="col-12 text-center">
    <p>Scandiweb Test Assignment</p>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>