<?php

interface Product {
    public function __construct();
    public function addData($sku, $name, $price, $type);
}