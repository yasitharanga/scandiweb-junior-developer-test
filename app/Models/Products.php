<?php
    class Products
    {
        public function getAllProducts()
        {
            $db = new Database();
            $sql = 'SELECT * FROM products';
            $result = $db->getConnection()->query($sql);
            $numRows = $result->num_rows;
            if ($numRows > 0) {
                while ($row = $result->fetch_assoc()) {
                    $data[] = $row;
                }

                return $data;
            } else {
                echo '<h4>No Records Found!</h4>';
            }
        }

        public function showAllProducts()
        {

            $datas = $this->getAllProducts();
            if ($datas) {
                foreach ($datas as $data) {
                    ?>
                        <div class="col-3 mb-3">
                            <div class="card">
                                <input type="checkbox" class="delete-checkbox m-2" name="delete-checkbox[]" value="<?php echo $data['sku']; ?>">
                                <div class="card-body text-center">
                                    <p class="card-title"><?php echo $data['sku']; ?></p>
                                    <p class="card-title"><?php echo $data['name']; ?></p>
                                    <p class="card-title"><?php echo $data['price']; ?> $</p>
                                    <p class="card-title"><?php echo $data['measurement'];
                                        echo ': ';
                                        echo $data['attribute'];
                                        echo ' ';
                                        echo $data['suffix']; ?></p>
                                </div>
                            </div>
                        </div>
                    <?php
                }
            }
        }
        
    }