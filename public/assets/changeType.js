$selector = document.querySelector(".selectType");

const getType = function(selectedValue) {
        const getDvd = function(){   
            return $selector.innerHTML = `
                    <div class="form-group my-3" id="attributes">
                        <label for="size">Size (MB)</label>
                        <input type="number" class="form-control" name="size" id="size" required>
                        <label for="size">Please provide DVD size in MB</label>
                    </div>
            `;           
        }
        const getBook = function(){
            return $selector.innerHTML = `
                    <div class="form-group my-3" id="attributes">
                        <label for="weight">Weight (KG)</label>
                        <input type="number" step="0.01" class="form-control" name="weight" id="weight" required>
                        <label for="size">Please provide Book weight in KG</label>
                    </div>
            `;
        }
        const getFurniture = function(){
            return $selector.innerHTML = `
                    <div class="form-group my-3" id="attributes">
                            <label for="height">Height (CM)</label>
                            <input type="number" class="form-control" name="height" id="height" required>
                            
                            <label for="width">Width (CM)</label>
                            <input type="number" class="form-control" name="width" id="width" required>
                            
                            <label for="length">Length (CM)</label>
                            <input type="number" class="form-control" name="length" id="length" required>
        
                            <label>Please provide Furniture Dimension in CM</label>
                    </div>
            `;
        }
        const type = {
            DVD: getDvd,
            Book: getBook,
            Furniture: getFurniture,
            default: function(){
               return "Type is Not Selected";
            }
        } 
        return (type[selectedValue] || type.default)();
    }
